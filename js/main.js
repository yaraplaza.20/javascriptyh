// Practica 01
// Tomar el contenido de la caja de texto
// Primer paso es: Tomar los valores

const btnEnviar = document.getElementById('btnEnviar');
const btnLimpiar = document.getElementById('btnLimpiar');
btnEnviar.addEventListener('click', mostrar);

function mostrar()
{
    const txtNombre = document.getElementById('idNombre').value; 
    const txtMensaje = document.getElementById('idSalida');
    txtMensaje.value =txtNombre;
}

btnLimpiar.addEventListener('click', limpiar);

function limpiar()
{
    const txtNombre = document.getElementById('idNombre'); 
    const txtMensaje = document.getElementById('idSalida');
    txtMensaje.value ='';
    txtNombre.value = '';
}






const btnCalcular = document.getElementById('btnCalcular');
const btnLimpiarIMC = document.getElementById('btnLimpiarIMC');

btnCalcular.addEventListener ('click', calcular);

function calcular()
{
    txtAltura = document.getElementById('idAltura').value;
    txtPeso = document.getElementById('idPeso').value;

    let altura = parseFloat(txtAltura);
    let peso = parseFloat(txtPeso);
    let imc  = 0;
    imc = peso / (altura*altura);
    document.getElementById('idResultado').value=imc.toFixed(2);

    // if (txtAltura.value !=null || txtPeso.value !=null)
    // {
    //     let altura = parseFloat (txtAltura);
    //     let peso = parseFloat (txtPeso);
    //     let imc = 0;
    //     imc = peso/(altura*altura)

    //     document.getElementById ('idResultado').value = imc.toFixed(0);
    // }
    // else
    // {
    //     alert ('Falta informacion');
    // }   
}

btnLimpiarIMC.addEventListener('click', limpiarIMC);

function limpiarIMC()
{
    document.getElementById('idAltura').value ='';
    document.getElementById('idPeso').value = '';
    document.getElementById('idResultado').value='';
}